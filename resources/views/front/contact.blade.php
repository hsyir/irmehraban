@extends('layouts.app')

@section('content')

    <section class="vh-100 bg2 pt-5" >
        <div class="container">
            <div class="card">
                <div class="card header">

                </div>
                <div class="card-body">
                    <h3 class="h3">
                        کمیته امداد کاشمر؛ واحد اکرام ایتام و محسنین
                    </h3>
                    <div>
                        <strong>شماره های تماس:</strong>
                        <ul>
                            <li><a href="tel:05155254370">05155254370</a></li>
                            <li><a href="tel:05155254370">05155254371</a></li>
                            <li><a href="tel:05155254370">09000000000</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
